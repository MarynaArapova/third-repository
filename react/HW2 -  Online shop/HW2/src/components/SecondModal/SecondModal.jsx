import React from "react";
import ModalBody from '../Modal/ModalBody.jsx';
import ModalClose from '../Modal/ModalClose.jsx';
import ModalFooter from '../Modal/ModalFooter.jsx';
import ModalHeader from '../Modal/ModalHeader.jsx';
import ModalWrapper from '../Modal/ModalWrapper.jsx';
import ModalContent from '../Modal/ModalContent.jsx';


const SecondModal = ({ onClose }) => {
    return (
        <ModalWrapper onClick={onClose}>
            <ModalBody>
                <ModalHeader><ModalClose onClick={onClose} /></ModalHeader>
                <ModalContent><h1>Add Product “NAME”</h1>Description for you product</ModalContent>
                <ModalFooter firstText="ADD TO FAVORITE">
                </ModalFooter>
            </ModalBody>
        </ModalWrapper >
    )
}

export default SecondModal
