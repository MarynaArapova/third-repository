import React from "react";
import "./Modal.scss"

const ModalFooter = (props) => {
    const { firstText, secondaryText, setCartItems, cartItems } = props;
    console.log(cartItems);

    return (
        <div className="modal-footer">

            {firstText &&
                <button
                    className="btn1"
                    onClick={() => {
                        setCartItems((count) => count + 1)
                    }}>

                    {firstText}
                </button>}

            {secondaryText &&
                <button
                    className="btn2"
                >
                    {secondaryText}</button>}
        </div>
    )
}

export default ModalFooter;
