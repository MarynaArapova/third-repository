import React from "react";
import PropTypes from "prop-types";

import cartLogo from './img/cart.png?react';
import favoriteLogo from './img/favorite.png?react';
import Logo from '../../img/logo.png?react'

import './PageHeader.scss'
const PageHeader = ({ cartItems, favoriteItems }) => {

    return (
        <header className="page-header">
            <img src={Logo} width='150px' height='70px' />
            <nav>
                <img src={cartLogo} width='70px' height='50px' />
                <span>{cartItems}</span>
                <img src={favoriteLogo} width='50px' height='50px' />
                <span>{favoriteItems}</span>
            </nav>
        </header>
    )
}

PageHeader.propTypes = {
    favoriteItems: PropTypes.number,
    cartItems: PropTypes.number,
}

export default PageHeader
