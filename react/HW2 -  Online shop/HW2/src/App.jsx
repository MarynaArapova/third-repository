import React from 'react';
import { useState, useEffect } from 'react'
import ProductsList from './components/ProductsList/ProductsList.jsx';
import { sendRequest } from './helpers/sendRequest.js';

import './App.scss'
import PageHeader from './components/PageHeader/PageHeader.jsx';

function App() {

  const [showFirstModal, setShowFirstModal] = useState(false);
  const [productsList, setProductsList] = useState([]);

  const [cartItems, setCartItems] = useState(0);
  const [favoriteItems, setFavoriteItems] = useState(0);



  const handleCloseFirstModal = () => {
    setShowFirstModal(false);
  };


  useEffect(() => {
    sendRequest('../public/products.json')
      .then((data) => { setProductsList(data) })
  }, []);


  return (
    <div>
      <PageHeader cartItems={cartItems} favoriteItems={favoriteItems} />
      <ProductsList productsList={productsList} setCartItems={setCartItems} setFavoriteItems={setFavoriteItems} favoriteItems={favoriteItems} cartItems={cartItems}
      />
    </div>
  )
}

export default App
