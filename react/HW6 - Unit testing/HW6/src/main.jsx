import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import App from './App.jsx'
import './index.scss'
import store from './store/index.js'
import { Provider } from 'react-redux'
import { ViewProvider } from './context/context.jsx';


ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <Provider store={store}>
            <ViewProvider>

                <BrowserRouter>
                    <App />
                </BrowserRouter>
            </ViewProvider>

        </Provider>
    </React.StrictMode>,
)
