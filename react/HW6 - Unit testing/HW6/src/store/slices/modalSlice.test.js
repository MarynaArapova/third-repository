import modalReducer, { openModal, closeModal } from './modalSlice';

describe('modalSlice', () => {
    const initialState = { isOpen: false };

    it('should handle initial state', () => {
        expect(modalReducer(undefined, { type: 'unknown' })).toEqual(initialState);
    });

    it('should handle openModal', () => {
        const actual = modalReducer(initialState, openModal());
        expect(actual.isOpen).toEqual(true);
    });

    it('should handle closeModal', () => {
        const actual = modalReducer({ isOpen: true }, closeModal());
        expect(actual.isOpen).toEqual(false);
    });
});
