import productsReducer, { fetchProducts } from './productsSlice';
import { configureStore } from '@reduxjs/toolkit';

describe('productsSlice', () => {
    const initialState = {
        data: [],
        loading: false,
        error: null,
    };

    it('should handle initial state', () => {
        expect(productsReducer(undefined, { type: 'unknown' })).toEqual(initialState);
    });

    it('should handle fetchProducts.pending', () => {
        const action = { type: fetchProducts.pending.type };
        const state = productsReducer(initialState, action);
        expect(state).toEqual({
            data: [],
            loading: true,
            error: null,
        });
    });

    it('should handle fetchProducts.fulfilled', () => {
        const action = {
            type: fetchProducts.fulfilled.type,
            payload: [{ id: 1, name: 'Product 1' }],
        };
        const state = productsReducer(initialState, action);
        expect(state).toEqual({
            data: [{ id: 1, name: 'Product 1' }],
            loading: false,
            error: null,
        });
    });

    it('should handle fetchProducts.rejected', () => {
        const action = {
            type: fetchProducts.rejected.type,
            error: { message: 'Fetch failed' },
        };
        const state = productsReducer(initialState, action);
        expect(state).toEqual({
            data: [],
            loading: false,
            error: 'Fetch failed',
        });
    });
});
