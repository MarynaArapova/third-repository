import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import { useDispatch, useSelector } from "react-redux";


const Product = ({ data, setCartItems, setFavoriteItems, favoriteItems, cartItems, openModalHandler }) => {

    const [inFavorite, setInFavorite] = useState(data.inFavorite || false);


    const toggleFavorite = () => {
        const updatedInFavorite = !inFavorite;
        setInFavorite(updatedInFavorite);

        let updatedFavoriteItems;
        if (updatedInFavorite) {
            updatedFavoriteItems = [...favoriteItems, data];
        } else {
            updatedFavoriteItems = favoriteItems.filter(item => item.article !== data.article);
        }

        setFavoriteItems(updatedFavoriteItems);
        localStorage.setItem('favorites', JSON.stringify(updatedFavoriteItems));
    };

    useEffect(() => {
        const savedFavorites = JSON.parse(localStorage.getItem('favorites')) || [];
        const isFavorite = savedFavorites.some(item => item.article === data.article);
        setInFavorite(isFavorite);
    }, [data.article]);

    const {
        name,
        price,
        url,
        color,
    } = data


    return (
        <li className="products-list-item">
            <img src={url} className="products-list-item-img" />
            <h1>{name} <br /><span>{price} грн</span></h1>
            <p>Колір: {color}</p>
            <div className="buttons-wrapper">
                <Button onClick={() => openModalHandler(data)} type='button' className="add-to-cart-btn" >У кошик</Button>
                <Button onClick={toggleFavorite} className={`favorite-btn`}>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                        <path fill={inFavorite ? 'black' : 'none'} stroke="black" strokeWidth="2" d="M12 1.154l2.478 7.618h8.022l-6.477 4.707 2.478 7.618-6.477-4.707-6.477 4.707 2.478-7.618-6.477-4.707h8.022z" />
                    </svg>
                </Button>
            </div>
        </li>
    )
}

Product.propTypes = {
    data: PropTypes.object,
    setCartItems: PropTypes.func,
    setFavoriteItems: PropTypes.func,
    favoriteItems: PropTypes.array,
    cartItems: PropTypes.array,
}

export default Product;
