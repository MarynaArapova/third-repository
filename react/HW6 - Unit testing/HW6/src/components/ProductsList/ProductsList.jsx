import React, { useState, useContext } from "react";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import FirstModal from "../FirstModal/FirstModal";
import { openModal, closeModal } from "../../store/slices/modalSlice";
import Product from "./Product";
import './ProductsList.scss';
import { ViewContext } from "../../context/context";
import Button from "../Button/Button";

const ProductsList = ({ setCartItems, setFavoriteItems, favoriteItems, cartItems, }) => {

    const productsList = useSelector(state => state.products.data);
    const dispatch = useDispatch();
    const { view, setView } = useContext(ViewContext);


    const [selectedProduct, setSelectedProduct] = useState(null);

    const isModalOpen = useSelector(state => state.modal.isOpen);
    const openModalHandler = (product) => {
        setSelectedProduct(product);
        dispatch(openModal());
    };
    const closeModalHandler = () => {
        setSelectedProduct(null);
        dispatch(closeModal());
    };

    const addToCartHandler = () => {
        const isAlreadyInCart = cartItems.some(item => item.article === selectedProduct.article);

        if (isAlreadyInCart) {
            console.log('already in the cart', selectedProduct.article);
        } else {
            const updatedCartItems = [...cartItems, selectedProduct];
            setCartItems(updatedCartItems);
            localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
        }
    }



    return (
        <div>
            <div className="view-switcher">
                <Button onClick={() => setView('grid')}>Grid View</Button>
                <Button onClick={() => setView('table')}>Table View</Button>
            </div>
            <ul className={`products-list ${view}`}>
                {productsList?.map((product, index) =>
                    <Product
                        data={product}
                        key={index}
                        setCartItems={setCartItems}
                        setFavoriteItems={setFavoriteItems}
                        favoriteItems={favoriteItems}
                        cartItems={cartItems}
                        openModalHandler={openModalHandler}
                    />)}
            </ul>
            {isModalOpen && selectedProduct && (
                <FirstModal
                    onClose={closeModalHandler}
                    image={selectedProduct.url}
                    setCartItems={setCartItems}
                    cartItems={cartItems}
                    onAdd={addToCartHandler}
                />
            )}
        </div>
    )
        ;
}



ProductsList.propTypes = {
    productsList: PropTypes.array,
    setCartItems: PropTypes.func,
    setFavoriteItems: PropTypes.func,
    favoriteItems: PropTypes.array,
    cartItems: PropTypes.array,
}

export default ProductsList