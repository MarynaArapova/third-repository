import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import SecondModal from './SecondModal';

describe('SecondModal Component', () => {
    const onCloseMock = jest.fn();
    const onDeleteMock = jest.fn();
    const imgMock = 'https://via.placeholder.com/150';

    it('renders the modal with the correct content', () => {
        render(
            <SecondModal
                onClose={onCloseMock}
                img={imgMock}
                onDelete={onDeleteMock}
            />
        );

        expect(screen.getByText(/Delete product\?/i)).toBeInTheDocument();
        expect(screen.getByAltText('img-of-deleted-product')).toHaveAttribute('src', imgMock);
    });


});
