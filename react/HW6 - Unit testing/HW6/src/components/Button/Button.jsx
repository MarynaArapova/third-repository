import React from "react";
import PropTypes from "prop-types";

import './Button.scss'

const Button = (props) => {
    const { type, className, onClick, children } = props;
    return (
        <>
            <button
                type={type}
                className={className}
                onClick={onClick}>
                {children}
            </button>
        </>
    )
}

Button.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    children: PropTypes.any,
}


export default Button;