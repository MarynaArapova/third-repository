import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import Button from './Button';

describe('Button Component', () => {
    it('renders the button with the correct text', () => {
        render(<Button>Click Me</Button>);
        expect(screen.getByText(/Click Me/i)).toBeInTheDocument();
    });

    it('applies the correct type and class', () => {
        const { container } = render(<Button type="submit" className="test-class">Submit</Button>);
        const button = container.querySelector('button');
        expect(button).toHaveAttribute('type', 'submit');
        expect(button).toHaveClass('test-class');
    });

    it('calls the onClick handler when clicked', () => {
        const handleClick = jest.fn();
        render(<Button onClick={handleClick}>Click Me</Button>);
        fireEvent.click(screen.getByText(/Click Me/i));
        expect(handleClick).toHaveBeenCalledTimes(1);
    });
});
