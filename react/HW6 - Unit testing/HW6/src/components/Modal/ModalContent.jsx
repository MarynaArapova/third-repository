import React from "react";
import "./Modal.scss"

const ModalContent = ({ children }) => {
    return (
        <div className="modal-content">
            {children}
        </div>
    )
}

export default ModalContent;