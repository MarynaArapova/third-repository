import React from "react";
import "./Modal.scss"

const ModalClose = ({ onClick }) => {

    return (
        <button className="modal-close" aria-label="close" onClick={onClick}>
            X
        </button>
    )
}

export default ModalClose;