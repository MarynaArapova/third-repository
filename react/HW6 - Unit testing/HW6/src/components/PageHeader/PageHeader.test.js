import React from 'react';
import { render } from '@testing-library/react';
import renderer from 'react-test-renderer';
import { BrowserRouter as Router } from 'react-router-dom';
import PageHeader from './PageHeader';

test('PageHeader component renders correctly', () => {
    const tree = renderer
        .create(
            <Router>
                <PageHeader cartItems={[]} favoriteItems={[]} />
            </Router>
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});
