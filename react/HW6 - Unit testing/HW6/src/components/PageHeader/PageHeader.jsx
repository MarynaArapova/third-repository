import React, { useEffect, useState } from "react";

import cartLogo from './img/cart.png?react';
import favoriteLogo from './img/favorite.png?react';
import Logo from '../../img/logo.png?react'

import './PageHeader.scss'
import { NavLink } from "react-router-dom";

const PageHeader = ({ cartItems, favoriteItems }) => {

    return (
        <header className="page-header">
            <NavLink to='/'>
                <img src={Logo} width='150px' height='70px' />

            </NavLink>
            <nav>
                <NavLink to="/cart">
                    <img className="cart" src={cartLogo} width='70px' height='50px' />
                    <span>{cartItems.length}</span>
                </NavLink>
                <NavLink to='/favorites'>
                    <img src={favoriteLogo} width='50px' height='50px' />
                    <span>{favoriteItems.length}</span>
                </NavLink>

            </nav>
        </header>
    )
}

// PageHeader.propTypes = {
//     favoriteItems: PropTypes.array,
//     cartItems: PropTypes.array,
// }

export default PageHeader
