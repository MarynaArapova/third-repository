import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import FirstModal from './FirstModal';

describe('FirstModal Component', () => {
    const onCloseMock = jest.fn();
    const onAddMock = jest.fn();
    const setCartItemsMock = jest.fn();
    const cartItemsMock = [];
    const imageMock = 'https://example.com/image.jpg';

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('calls onClose when close button is clicked', () => {

        render(
            <FirstModal
                onClose={onCloseMock}
                image={imageMock}
                setCartItems={setCartItemsMock}
                cartItems={cartItemsMock}
                onAdd={onAddMock}
            />
        );

        fireEvent.click(screen.getByLabelText(/close/i));
        expect(onCloseMock).toHaveBeenCalledTimes(2);
    });

    it('calls onClose when "Ні" button is clicked', () => {
        render(
            <FirstModal
                onClose={onCloseMock}
                image={imageMock}
                setCartItems={setCartItemsMock}
                cartItems={cartItemsMock}
                onAdd={onAddMock}
            />
        );

        fireEvent.click(screen.getByText(/Ні/i));
        expect(onCloseMock).toHaveBeenCalledTimes(1);
    });
});
