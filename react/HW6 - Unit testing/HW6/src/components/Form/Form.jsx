import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import PropTypes from "prop-types";

import './Form.scss'

const Form = ({ cartItems, setCartItems }) => {
    const validationSchema = Yup.object({
        name: Yup.string().required("Name is required."),
        lastName: Yup.string().required("Last name is required."),
        age: Yup.number().min(0, "Age cannot be negative.").required("Age is required."),
        address: Yup.string().required("Address is required."),
        phoneNumber: Yup.string()
            .matches(/^[0-9]{10}$/, "Phone number must be exactly 10 digits.")
            .required("Phone number is required."),
    });

    const formik = useFormik({
        initialValues: {
            name: "",
            lastName: "",
            age: "",
            address: "",
            phoneNumber: ""
        },
        validationSchema,
        onSubmit: (values, { resetForm }) => {
            alert("Purchase completed successfully!");
            console.log("Form Data Submitted: ", values);
            console.log("Ordered products: ", cartItems);

            const orderInfo = {
                person: values,
                order: cartItems
            };
            console.log('info', orderInfo);

            cartClear();
            setCartItems([]);
            resetForm();
        }
    });

    const cartClear = () => {
        localStorage.removeItem('cartItems');
    };

    return (
        <form onSubmit={formik.handleSubmit}>
            <label>
                Name:
                <input
                    type="text"
                    name="name"
                    value={formik.values.name}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    required
                />
                {formik.touched.name && formik.errors.name && <span className="error">{formik.errors.name}</span>}
            </label>
            <label>
                Last name:
                <input
                    type="text"
                    name="lastName"
                    value={formik.values.lastName}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    required
                />
                {formik.touched.lastName && formik.errors.lastName && <span className="error">{formik.errors.lastName}</span>}
            </label>
            <label>
                Age:
                <input
                    type="number"
                    name="age"
                    value={formik.values.age}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    min="0"
                    required
                />
                {formik.touched.age && formik.errors.age && <span className="error">{formik.errors.age}</span>}
            </label>
            <label>
                Address:
                <input
                    type="text"
                    name="address"
                    value={formik.values.address}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    required
                />
                {formik.touched.address && formik.errors.address && <span className="error">{formik.errors.address}</span>}
            </label>
            <label>
                Phone number:
                <input
                    type="tel"
                    name="phoneNumber"
                    value={formik.values.phoneNumber}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    required
                />
                {formik.touched.phoneNumber && formik.errors.phoneNumber && <span className="error">{formik.errors.phoneNumber}</span>}
            </label>
            <button type="submit">Checkout</button>
        </form>
    );
};

Form.propTypes = {
    cartItems: PropTypes.array.isRequired,
    setCartItems: PropTypes.func.isRequired
};

export default Form;


// import React, { useState } from "react";

// const Form = ({ cartItems, setCartItems }) => {
//     const [formData, setFormData] = useState({
//         name: "",
//         lastName: "",
//         age: "",
//         address: "",
//         phoneNumber: ""
//     });

//     const [touched, setTouched] = useState({
//         name: false,
//         lastName: false,
//         age: false,
//         address: false,
//         phoneNumber: false
//     });

//     const [errors, setErrors] = useState({
//         name: "",
//         lastName: "",
//         age: "",
//         address: "",
//         phoneNumber: ""
//     });

//     // Функція обробки змін у полях форми
//     const handleChange = (e) => {
//         const { name, value } = e.target;

//         setFormData(prevData => ({ ...prevData, [name]: value }));

//         validateField(name, value);
//     };

//     const handleBlur = (e) => {
//         const { name } = e.target;

//         setTouched(prevTouched => ({ ...prevTouched, [name]: true }));

//         validateField(name, formData[name]);
//     };

//     const validateField = (name, value) => {
//         let errorMsg = "";

//         switch (name) {
//             case "name":
//                 if (!value) errorMsg = "Name is required.";
//                 break;
//             case "lastName":
//                 if (!value) errorMsg = "Last name is required.";
//                 break;
//             case "age":
//                 if (!value) errorMsg = "Age is required.";
//                 else if (value < 0) errorMsg = "Age cannot be negative.";
//                 break;
//             case "address":
//                 if (!value) errorMsg = "Address is required.";
//                 break;
//             case "phoneNumber":
//                 const phonePattern = /^[0-9]{10}$/;
//                 if (!value) errorMsg = "Phone number is required.";
//                 else if (!phonePattern.test(value)) errorMsg = "Phone number must be exactly 10 digits.";
//                 break;
//             default:
//                 break;
//         }

//         setErrors(prevErrors => ({ ...prevErrors, [name]: errorMsg }));
//     };

//     const handleSubmit = (e) => {
//         e.preventDefault();

//         Object.keys(formData).forEach(key => {
//             validateField(key, formData[key]);
//             setTouched(prevTouched => ({ ...prevTouched, [key]: true }));
//         });

//         const hasErrors = Object.values(errors).some(error => error !== "");

//         if (!hasErrors) {
//             alert("Purchase completed successfully!");
//             console.log("Form Data Submitted: ", formData);
//             console.log("Ordered products: ", cartItems);


//             setFormData({
//                 name: "",
//                 lastName: "",
//                 age: "",
//                 address: "",
//                 phoneNumber: ""
//             });

//             setTouched({
//                 name: false,
//                 lastName: false,
//                 age: false,
//                 address: false,
//                 phoneNumber: false
//             });

//             setErrors({
//                 name: "",
//                 lastName: "",
//                 age: "",
//                 address: "",
//                 phoneNumber: ""
//             });
//         };
//         cartClear()
//         setCartItems([]);

//     };

//     //

//     const cartClear = () => {
//         localStorage.removeItem('cartItems')
//     }
//     //

//     return (
//         <form onSubmit={handleSubmit}>
//             <label>
//                 Name:
//                 <input
//                     type="text"
//                     name="name"
//                     value={formData.name}
//                     onChange={handleChange}
//                     onBlur={handleBlur}
//                     required
//                 />
//                 {touched.name && errors.name && <span className="error">{errors.name}</span>}
//             </label>
//             <label>
//                 Last name:
//                 <input
//                     type="text"
//                     name="lastName"
//                     value={formData.lastName}
//                     onChange={handleChange}
//                     onBlur={handleBlur}
//                     required
//                 />
//                 {touched.lastName && errors.lastName && <span className="error">{errors.lastName}</span>}
//             </label>
//             <label>
//                 Age:
//                 <input
//                     type="number"
//                     name="age"
//                     value={formData.age}
//                     onChange={handleChange}
//                     onBlur={handleBlur}
//                     min="0"
//                     required
//                 />
//                 {touched.age && errors.age && <span className="error">{errors.age}</span>}
//             </label>
//             <label>
//                 Address:
//                 <input
//                     type="text"
//                     name="address"
//                     value={formData.address}
//                     onChange={handleChange}
//                     onBlur={handleBlur}
//                     required
//                 />
//                 {touched.address && errors.address && <span className="error">{errors.address}</span>}
//             </label>
//             <label>
//                 Phone number:
//                 <input
//                     type="tel"
//                     name="phoneNumber"
//                     value={formData.phoneNumber}
//                     onChange={handleChange}
//                     onBlur={handleBlur}
//                     pattern="[0-9]{10}"
//                     required
//                 />
//                 {touched.phoneNumber && errors.phoneNumber && <span className="error">{errors.phoneNumber}</span>}
//             </label>
//             <button type="submit">Checkout</button>
//         </form>
//     );
// }

// export default Form;
