import React, { createContext, useState } from 'react';

const ViewContext = createContext();

const ViewProvider = ({ children }) => {
    const [view, setView] = useState('grid'); // 'grid' или 'table'

    return (
        <ViewContext.Provider value={{ view, setView }}>
            {children}
        </ViewContext.Provider>
    );
};

export { ViewContext, ViewProvider };
