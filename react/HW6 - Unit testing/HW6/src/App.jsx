import React from 'react';
import { useState, useEffect } from 'react'
import RootRoters from './routers'
import './App.scss'
import PageHeader from './components/PageHeader/PageHeader.jsx';
import { useSelector } from 'react-redux';

function App() {

  const productsList = useSelector(state => state.products.data);

  const [cartItems, setCartItems] = useState(() => {
    const savedCartItems = localStorage.getItem('cartItems');
    return savedCartItems ? JSON.parse(savedCartItems) : [];
  });

  const [favoriteItems, setFavoriteItems] = useState(() => {
    const savedFavoriteItems = localStorage.getItem('favorites');
    return savedFavoriteItems ? JSON.parse(savedFavoriteItems) : [];
  });

  return (
    <>
      <PageHeader
        cartItems={cartItems}
        favoriteItems={favoriteItems} />
      <RootRoters
        cartItems={cartItems}
        favoriteItems={favoriteItems}
        setCartItems={setCartItems}
        setFavoriteItems={setFavoriteItems}
        productsList={productsList}
      />
    </>
  )
}

export default App
