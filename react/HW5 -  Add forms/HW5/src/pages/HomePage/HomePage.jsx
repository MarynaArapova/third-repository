import React, { useEffect } from "react"
import ProductsList from "../../components/ProductsList/ProductsList"
import { useDispatch, useSelector } from 'react-redux';
import { fetchProducts } from "../../store/slices/productsSlice";


const HomePage = ({ cartItems, favoriteItems, setCartItems, setFavoriteItems, productsList }) => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch]);

    return (
        <div>

            <ProductsList
                productsList={productsList}
                setCartItems={setCartItems}
                setFavoriteItems={setFavoriteItems}
                favoriteItems={favoriteItems}
                cartItems={cartItems}
            />
        </div>
    )
}

export default HomePage