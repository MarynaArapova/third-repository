import React from "react";

import './FavoritesPage.scss'
import FavoriteProduct from "./FavoriteProduct.jsx";

const FavoritesPage = ({ favoriteItems, setFavoriteItems, }) => {

    return (
        <>
            <h1 className="favorites-page-title">My Favorites</h1>
            <ul className="favorites-list">
                {favoriteItems?.map((item, index) => (
                    <FavoriteProduct
                        data={item}
                        key={index}
                        favoriteItems={favoriteItems}
                        setFavoriteItems={setFavoriteItems}
                    />
                ))}
            </ul>
        </>)
}

export default FavoritesPage