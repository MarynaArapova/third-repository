import React, { useState, useEffect } from "react"
import CartProduct from "./CartProduct";

import './CartPage.scss'
import Form from "../../components/Form/Form";

const CartPage = ({ cartItems, setCartItems, onDelete }) => {

    return (
        <>
            <h1 className="cart-page-title">My Cart</h1>
            <Form cartItems={cartItems} setCartItems={setCartItems} />
            <ul className="cart-list">
                {cartItems?.map((product, index) => {
                    return <CartProduct
                        data={product}
                        key={index}
                        onDelete={onDelete}
                        cartItems={cartItems}
                        setCartItems={setCartItems} />
                })}
            </ul>
        </>
    )
}

export default CartPage