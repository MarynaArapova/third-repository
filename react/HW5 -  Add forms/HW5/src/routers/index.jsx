import { Routes, Route } from 'react-router-dom';
import CartPage from '../pages/CartPage/CartPage.jsx';
import HomePage from '../pages/HomePage/HomePage.jsx';
import FavoritesPage from '../pages/FavoritesPage/FavoritesPage.jsx'

export default ({ cartItems, favoriteItems, setCartItems, setFavoriteItems, productsList
}) => {

    return (
        <Routes>
            <Route index element={<HomePage
                productsList={productsList}
                cartItems={cartItems}
                favoriteItems={favoriteItems}
                setCartItems={setCartItems}
                setFavoriteItems={setFavoriteItems}
            />}>
            </Route>
            <Route path="/cart" element={<CartPage
                cartItems={cartItems}
                setCartItems={setCartItems}
                favoriteItems={favoriteItems}
            />} />
            <Route path='/favorites' element={
                <FavoritesPage
                    favoriteItems={favoriteItems}
                    setFavoriteItems={setFavoriteItems}
                />
            } />
            <Route path="*" element={<div>404</div>} />
        </Routes>
    )
}
