import React from "react";
import "./Modal.scss"

const ModalBody = ({ children }) => {

    return (
        <div className="modal-body">
            {children}
        </div>
    )
}

export default ModalBody;