import React, { useState } from "react";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import FirstModal from "../FirstModal/FirstModal";
import { openModal, closeModal } from "../../store/slices/modalSlice";
import Product from "./Product";
import './ProductsList.scss'

const ProductsList = ({ setCartItems, setFavoriteItems, favoriteItems, cartItems, }) => {

    const productsList = useSelector(state => state.products.data);
    const dispatch = useDispatch();

    const [selectedProduct, setSelectedProduct] = useState(null);

    const isModalOpen = useSelector(state => state.modal.isOpen);
    const openModalHandler = (product) => {
        setSelectedProduct(product);
        dispatch(openModal());
    };
    const closeModalHandler = () => {
        setSelectedProduct(null);
        dispatch(closeModal());
    };

    const addToCartHandler = () => {
        const isAlreadyInCart = cartItems.some(item => item.article === selectedProduct.article);

        if (isAlreadyInCart) {
            console.log('already in the cart', selectedProduct.article);
        } else {
            const updatedCartItems = [...cartItems, selectedProduct];
            setCartItems(updatedCartItems);
            localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
        }
    }



    return (
        <ul className="products-list">
            {productsList?.map((product, index) =>
                <Product
                    data={product}
                    key={index}
                    setCartItems={setCartItems}
                    setFavoriteItems={setFavoriteItems}
                    favoriteItems={favoriteItems}
                    cartItems={cartItems}
                    openModalHandler={openModalHandler}

                />)}

            {isModalOpen && selectedProduct && (
                <FirstModal
                    onClose={closeModalHandler}
                    image={selectedProduct.url}
                    setCartItems={setCartItems}
                    cartItems={cartItems}
                    onAdd={addToCartHandler}
                />
            )}
        </ul>
    )
        ;
}



ProductsList.propTypes = {
    productsList: PropTypes.array,
    setCartItems: PropTypes.func,
    setFavoriteItems: PropTypes.func,
    favoriteItems: PropTypes.array,
    cartItems: PropTypes.array,
}

export default ProductsList