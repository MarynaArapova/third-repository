import React from 'react';
import { useState } from 'react'

import Button from './components/Button/Button.jsx';
import FirstModal from './components/FirstModal/FirstModal.jsx';
import SecondModal from './components/SecondModal/SecondModal.jsx';


import './App.scss'

function App() {

  const [showFirstModal, setShowFirstModal] = useState(false);
  const [showSecondModal, setShowSecondModal] = useState(false);

  const handleCloseFirstModal = () => {
    setShowFirstModal(false);
  };

  const handleCloseSecondModal = () => {
    setShowSecondModal(false);
  };

  return (
    <div>
      {showFirstModal === false && showSecondModal === false && (
        <div className='hero-btns-wrapper'>
          <Button onClick={() => setShowFirstModal(!showFirstModal)} type='button' className="hero-btn">Open first modal</Button>
          <Button onClick={() => setShowSecondModal(!showSecondModal)} type='button' className="hero-btn">Open second modal</Button>
        </div>
      )}

      {showFirstModal && showSecondModal === false && (
        <FirstModal onClose={handleCloseFirstModal}></FirstModal>
      )}

      {showSecondModal && showFirstModal === false && (
        <SecondModal onClose={handleCloseSecondModal}></SecondModal>
      )}
    </div>
  )
}

export default App
