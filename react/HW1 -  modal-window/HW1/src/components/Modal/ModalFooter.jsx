import React from "react";
import "./Modal.scss"

const ModalFooter = (props) => {
    const { firstText, secondaryText, firstClick, secondaryClick } = props;
    return (
        <div className="modal-footer">
            {firstText && <button className="btn1" onClick={() => { alert('you pressed the purple button') }}>{firstText}</button>}
            {secondaryText && <button className="btn2" onClick={() => { alert('you pressed the white button') }}>{secondaryText}</button>}
        </div>
    )
}

export default ModalFooter;
