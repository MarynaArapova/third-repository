import React from "react";
import "./Modal.scss"

const ModalContent = ({ children }) => {
    return (
        <div className="modal-Content">
            {children}
        </div>
    )
}

export default ModalContent;