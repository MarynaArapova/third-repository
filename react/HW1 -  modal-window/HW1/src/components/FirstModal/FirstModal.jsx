import React from "react";
import ModalBody from '../Modal/ModalBody.jsx';
import ModalClose from '../Modal//ModalClose.jsx';
import ModalFooter from '../Modal//ModalFooter.jsx';
import ModalHeader from '../Modal//ModalHeader.jsx';
import ModalWrapper from '../Modal//ModalWrapper.jsx';
import ModalContent from '../Modal//ModalContent.jsx';
import image from '../../img/image.png'

const FirstModal = ({ onClose }) => {

    return (
        <ModalWrapper onClick={onClose}>
            <ModalBody>
                <ModalHeader><ModalClose onClick={onClose} /></ModalHeader>
                <ModalContent><img src={image} alt="" width='276' height='140px' /><h1>Product Delete!</h1>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</ModalContent>
                <ModalFooter firstText="NO, CANCEL"
                    secondaryText="YES, DELETE">
                </ModalFooter>
            </ModalBody>
        </ModalWrapper >
    )
}

export default FirstModal
