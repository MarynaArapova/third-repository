import React from "react";

import './Button.scss'

const Button = (props) => {
    const { type, className, onClick, children } = props;
    return (
        <>
            <button type={type}
                className={className}
                onClick={onClick}>
                {children}
            </button>
        </>
    )
}

export default Button;