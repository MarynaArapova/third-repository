import React, { useState } from "react";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import FirstModal from "../FirstModal/FirstModal";
import { openModal, closeModal } from "../../store/slices/modalSlice";
import Product from "./Product";
import './ProductsList.scss'

const ProductsList = ({ setCartItems, setFavoriteItems, favoriteItems, cartItems, isAddedToCart, setIsAddedToCart }) => {
    const productsList = useSelector(state => state.products.data);
    console.log(productsList);
    //
    const [selectedProduct, setSelectedProduct] = useState(null);
    const dispatch = useDispatch();

    const isModalOpen = useSelector(state => state.modal.isOpen);

    const openModalHandler = (product) => {
        setSelectedProduct(product);
        dispatch(openModal());
    };

    const closeModalHandler = () => {
        setSelectedProduct(null);
        dispatch(closeModal());
    };
    const addToCartHandler = () => {
        const cartItemsFromStorage = JSON.parse(localStorage.getItem('cartItems')) || [];
        const isAlreadyInCart = cartItemsFromStorage.some(item => item.article === selectedProduct.article);
        if (isAlreadyInCart) {
            console.log('already in the cart', selectedProduct.article);
        } else {
            const updatedCartItems = [...cartItemsFromStorage, selectedProduct];
            localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
            setCartItems(updatedCartItems.length);
            localStorage.setItem('numberOfCartItems', updatedCartItems.length);
            setIsAddedToCart(true)
        }
    };
    //
    return (
        <ul className="products-list">
            {productsList?.map((product, index) =>
                <Product
                    data={product}
                    key={index}
                    setCartItems={setCartItems}
                    setFavoriteItems={setFavoriteItems}
                    favoriteItems={favoriteItems}
                    cartItems={cartItems}
                    isAddedToCart={isAddedToCart}
                    setIsAddedToCart={setIsAddedToCart}
                    openModalHandler={openModalHandler}

                />)}
            {isModalOpen && selectedProduct && (
                <FirstModal
                    onClose={closeModalHandler}
                    image={selectedProduct.url}
                    setCartItems={setCartItems}
                    cartItems={cartItems}
                    onAdd={addToCartHandler}
                />
            )}

        </ul>
    )
}


ProductsList.propTypes = {
    productsList: PropTypes.array,
    setCartItems: PropTypes.func,
    setFavoriteItems: PropTypes.func,
    favoriteItems: PropTypes.number,
    cartItems: PropTypes.number,
}

export default ProductsList