import React from "react";
import "./Modal.scss"

const ModalWrapper = ({ children, onClick }) => {
    return (
        <div className="modal-wrapper" onClick={onClick}>
            {children}
        </div>
    )
}

export default ModalWrapper;