import React, { useEffect } from "react";
import "./Modal.scss"
import Button from "../Button/Button";

const ModalFooter = ({ firstText, secondaryText, setCartItems, cartItems, onClick }) => {
    return (
        <div className="modal-footer">
            {firstText &&
                <Button
                    className="btn1"
                    onClick={onClick}
                    children={firstText}
                />}

            {secondaryText &&
                <Button
                    className="btn2"
                    children={secondaryText}
                />}
        </div >
    )
}

export default ModalFooter;
