import React from "react";
import PropTypes from "prop-types";

import cartLogo from './img/cart.png?react';
import favoriteLogo from './img/favorite.png?react';
import Logo from '../../img/logo.png?react'

import './PageHeader.scss'
import { NavLink } from "react-router-dom";
const PageHeader = ({ cartItems, favoriteItems }) => {
    return (
        <header className="page-header">
            <NavLink to='/'>
                <img src={Logo} width='150px' height='70px' />

            </NavLink>
            <nav>
                <NavLink to="/cart">
                    <img className="cart" src={cartLogo} width='70px' height='50px' />
                    <span>{cartItems}</span>
                </NavLink>
                <NavLink to='/favorites'>
                    <img src={favoriteLogo} width='50px' height='50px' />
                    <span>{favoriteItems}</span>
                </NavLink>

            </nav>
        </header>
    )
}

PageHeader.propTypes = {
    favoriteItems: PropTypes.number,
    cartItems: PropTypes.number,
}

export default PageHeader
