import React from "react";
import ModalBody from '../Modal/ModalBody.jsx';
import ModalClose from '../Modal/ModalClose.jsx';
import ModalFooter from '../Modal/ModalFooter.jsx';
import ModalHeader from '../Modal/ModalHeader.jsx';
import ModalWrapper from '../Modal/ModalWrapper.jsx';
import ModalContent from '../Modal/ModalContent.jsx';

import './SecondModal.scss'
const SecondModal = ({ onClose, img, onDelete }) => {
    return (
        <ModalWrapper onClick={onClose}>
            <ModalBody>
                <ModalHeader><ModalClose onClick={onClose} /></ModalHeader>
                <ModalContent >
                    <div className='second-modal-content'><h1>Delete product?</h1>
                        <img src={img} className="img-of-deleted-product" />
                        Are you sure that you want to delete this product fron your cart?</div>
                </ModalContent>
                <ModalFooter
                    firstText="Yes, delete"
                    secondaryText='No'
                    onClick={onDelete}
                >
                </ModalFooter>
            </ModalBody>
        </ModalWrapper >
    )
}

export default SecondModal
