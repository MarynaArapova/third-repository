import { configureStore } from '@reduxjs/toolkit'
import productsReducer from './slices/productsSlice.js'
import modalReducer from './slices/modalSlice.js'

export default configureStore({
    reducer: {
        products: productsReducer,
        modal: modalReducer,
    },
})