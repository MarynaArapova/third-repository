import React, { useEffect } from "react"
import ProductsList from "../../components/ProductsList/ProductsList"
import { useDispatch, useSelector } from 'react-redux';
import { fetchProducts } from "../../store/slices/productsSlice";


const HomePage = ({ cartItems, favoriteItems, setCartItems, setFavoriteItems, isAddedToCart, setIsAddedToCart }) => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch]);

    const productsList = useSelector(state => state.products.data);
    return (
        <div>

            <ProductsList
                productsList={productsList}
                setCartItems={setCartItems}
                setFavoriteItems={setFavoriteItems}
                favoriteItems={favoriteItems}
                cartItems={cartItems}
                isAddedToCart={isAddedToCart}
                setIsAddedToCart={setIsAddedToCart}
            />
        </div>
    )
}

export default HomePage