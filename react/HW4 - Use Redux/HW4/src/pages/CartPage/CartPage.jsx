import React, { useState, useEffect } from "react"
import CartProduct from "./CartProduct";

import './CartPage.scss'

const CartPage = ({ cartItems, setCartItems, onDelete
}) => {
    const [cartList, setCartList] = useState([])

    useEffect(() => {
        const cartProductArticles = JSON.parse(localStorage.getItem('cartItems')) || [];
        setCartList(cartProductArticles);
    }, [cartItems]);

    return (
        <>
            <h1 className="cart-page-title">My Cart</h1>
            <ul className="cart-list">
                {cartList?.map((product, index) => {
                    return <CartProduct data={product} key={index} onDelete={onDelete} cartList={cartList}
                        setCartList={setCartList} setCartItems={setCartItems} />
                })}
            </ul>
        </>
    )
}

export default CartPage