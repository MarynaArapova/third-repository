import { Routes, Route } from 'react-router-dom';
import CartPage from '../pages/CartPage/CartPage.jsx';
import HomePage from '../pages/HomePage/HomePage.jsx';
import FavoritesPage from '../pages/FavoritesPage/FavoritesPage.jsx'

export default ({ cartItems, favoriteItems, productsList, setCartItems, setFavoriteItems, addToCart, setAddToCart, isAddedToCart, setIsAddedToCart, onDelete, isFavorite, setIsFavorite }) => {
    return (
        <Routes>
            <Route index element={<HomePage
                cartItems={cartItems}
                favoriteItems={favoriteItems}
                productsList={productsList}
                setCartItems={setCartItems}
                setFavoriteItems={setFavoriteItems}
                isAddedToCart={isAddedToCart}
                setIsAddedToCart={setIsAddedToCart}
            />}>
            </Route>
            <Route path="/cart" element={<CartPage cartItems={cartItems} favoriteItems={favoriteItems} addToCart={addToCart}
                setAddToCart={setAddToCart} productsList={productsList} isAddedToCart={isAddedToCart}
                setIsAddedToCart={setIsAddedToCart} onDelete={onDelete} setCartItems={setCartItems}
            />} />
            <Route path='/favorites' element={
                <FavoritesPage favoriteItems={favoriteItems}
                    setFavoriteItems={setFavoriteItems} isFavorite={isFavorite}
                    setIsFavorite={setIsFavorite} productsList={productsList} />
            } />
            <Route path="*" element={<div>404</div>} />
        </Routes>
    )
}
