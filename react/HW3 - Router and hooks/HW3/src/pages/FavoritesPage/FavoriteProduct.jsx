import React from "react";
import Button from "../../components/Button/Button";

const FavoriteProduct = ({ data, favoriteItems, setFavoriteItems, isFavorite, setIsFavorite }) => {
    const {
        name,
        price,
        url,
        color,
    } = data

    const removeFromFavorite = () => {
        const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
        const updatedFavorites = favorites.filter(item => item.article !== data.article);
        localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
        setIsFavorite(false);
        const newFavoriteItems = favoriteItems > 0 ? favoriteItems - 1 : 0;
        setFavoriteItems(newFavoriteItems);
        localStorage.setItem('numberOfFavorites', newFavoriteItems);

    }

    return (
        <li className="favorites-list-item">
            <img src={url} className="favorites-list-item-img" />
            <h1>{name} <br /><span>{price} грн</span></h1>
            <p>Колір: {color}</p>
            <Button onClick={() => removeFromFavorite()} className='favorite-btn active'>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                    <path fill='black' stroke="black" strokeWidth="2" d="M12 1.154l2.478 7.618h8.022l-6.477 4.707 2.478 7.618-6.477-4.707-6.477 4.707 2.478-7.618-6.477-4.707h8.022z" />
                </svg>
            </Button>
        </li >

    )
};

export default FavoriteProduct