import React from "react";

import './FavoritesPage.scss'
import FavoriteProduct from "./FavoriteProduct.jsx";

const FavoritesPage = ({ favoriteItems, setFavoriteItems, isFavorite, setIsFavorite, productsList }) => {
    const listOfFavorites = JSON.parse(localStorage.getItem('favorites'))
    console.log(listOfFavorites);
    return (
        <>
            <h1 className="favorites-page-title">My Favorites</h1>
            <ul className="favorites-list">
                {listOfFavorites?.map((item, index) => (
                    <FavoriteProduct data={item} key={index}
                        favoriteItems={favoriteItems}
                        setFavoriteItems={setFavoriteItems}
                        isFavorite={isFavorite}
                        setIsFavorite={setIsFavorite} />
                ))}
            </ul>
        </>)
}

export default FavoritesPage