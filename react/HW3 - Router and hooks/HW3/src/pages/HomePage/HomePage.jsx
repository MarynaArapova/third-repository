import React from "react"
import PageHeader from "../../components/PageHeader/PageHeader"
import ProductsList from "../../components/ProductsList/ProductsList"


const HomePage = ({ cartItems, favoriteItems, productsList, setCartItems, setFavoriteItems, isAddedToCart, setIsAddedToCart }) => {

    return (
        <div>

            <ProductsList
                productsList={productsList}
                setCartItems={setCartItems}
                setFavoriteItems={setFavoriteItems}
                favoriteItems={favoriteItems}
                cartItems={cartItems}
                isAddedToCart={isAddedToCart}
                setIsAddedToCart={setIsAddedToCart}
            />
        </div>
    )
}

export default HomePage