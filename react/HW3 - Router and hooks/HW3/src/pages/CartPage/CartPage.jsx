import React, { useState, useEffect } from "react"
import ProductsList from "../../components/ProductsList/ProductsList";
import CartProduct from "./CartProduct";

import './CartPage.scss'

const CartPage = ({ cartItems, setCartItems, favoriteItems, addToCart, setAddToCart, productsList, isAddedToCart, setIsAddedToCart, onDelete
}) => {
    const [cartList, setCartList] = useState([])

    useEffect(() => {
        const cartProductArticles = JSON.parse(localStorage.getItem('cartItems')) || [];
        setCartList(cartProductArticles);
        // setCartItems(cartProductArticles.length);
        // console.log(cartItems);
    }, [cartItems]);

    return (
        <>
            <h1 className="cart-page-title">My Cart</h1>
            <ul className="cart-list">
                {cartList?.map((product, index) => {
                    return <CartProduct data={product} key={index} onDelete={onDelete} cartList={cartList}
                        setCartList={setCartList} setCartItems={setCartItems} />
                })}
            </ul>
        </>
    )
}

export default CartPage