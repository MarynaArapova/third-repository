import React, { useState } from "react";
import Button from "../../components/Button/Button";
import SecondModal from "../../components/SecondModal/SecondModal";

const CartProduct = ({ data, cartList,
    setCartList, setCartItems }) => {
    const {
        name,
        price,
        url,
        color,
    } = data

    const [isActiveSecondModal, setIsActiveSecondModal] = useState(false)

    const handleCloseSecondModal = () => {
        setIsActiveSecondModal(false)
    }

    // const handleDeleteProduct = () => {
    //     const cartItemsFromStorage = JSON.parse(localStorage.getItem('cartItems')) || [];
    //     const updatedCartItems = cartItemsFromStorage.filter((item) => item.article !== data.article);
    //     localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
    // };

    const handleDeleteProduct = () => {
        const updatedCartList = cartList.filter((item) => item.article !== data.article);
        localStorage.setItem('cartItems', JSON.stringify(updatedCartList));
        setCartList(updatedCartList);
        localStorage.setItem('numberOfCartItems', updatedCartList.length)
        setCartItems(updatedCartList.length)
    }


    return (
        <>
            <li className="cart-item">
                <Button
                    className='delete-cart-product-btn'
                    children='X'
                    onClick={() => setIsActiveSecondModal(!isActiveSecondModal)}

                />

                <img src={url} className="cart-item-img" />
                <h1>{name} <br /><span>{price} грн</span></h1>
                <p>Колір: {color}</p>
            </li >
            {isActiveSecondModal && <SecondModal onClose={handleCloseSecondModal} img={url} onDelete={handleDeleteProduct} />}

        </>

    )
}

export default CartProduct