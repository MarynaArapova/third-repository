import React from "react";
import PropTypes from "prop-types";

import Product from "./Product";

import './ProductsList.scss'

const ProductsList = ({ productsList, setCartItems, setFavoriteItems, favoriteItems, cartItems, isAddedToCart, setIsAddedToCart }) => {
    return (
        <ul className="products-list">
            {productsList?.map((product, index) =>
                <Product
                    data={product}
                    key={index}
                    setCartItems={setCartItems}
                    setFavoriteItems={setFavoriteItems}
                    favoriteItems={favoriteItems}
                    cartItems={cartItems}
                    isAddedToCart={isAddedToCart}
                    setIsAddedToCart={setIsAddedToCart}
                />)}

        </ul>
    )
}


ProductsList.propTypes = {
    productsList: PropTypes.array,
    setCartItems: PropTypes.func,
    setFavoriteItems: PropTypes.func,
    favoriteItems: PropTypes.number,
    cartItems: PropTypes.number,
}

export default ProductsList