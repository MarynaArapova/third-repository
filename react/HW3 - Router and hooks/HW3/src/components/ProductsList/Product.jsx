import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";

import FirstModal from "../FirstModal/FirstModal";
import Button from "../Button/Button";

const Product = ({ data, setCartItems, setFavoriteItems, favoriteItems, cartItems, isAddedToCart, setIsAddedToCart }) => {


    const [addToCart, setAddToCart] = useState(false)
    const [isFavorite, setIsFavorite] = useState(false);

    const handleCloseFirstModal = () => {
        setAddToCart(false);
    };


    //favorite

    useEffect(() => {
        const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
        setIsFavorite(favorites.includes(data.article));
    }, []);

    useEffect(() => {
        const storedFavoriteItems = localStorage.getItem('numberOfFavorites');
        if (storedFavoriteItems !== null) {
            setFavoriteItems(parseInt(storedFavoriteItems));
        }
    }, []);

    useEffect(() => {
        const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
        setIsFavorite(favorites.some(item => item.article === data.article));
    }, [data.article]);

    const toggleFavorite = () => {
        const favorites = JSON.parse(localStorage.getItem('favorites')) || [];
        const alreadyInFavorite = favorites.some((item) => item.article === data.article)
        if (alreadyInFavorite) {
            console.log('already in favorite');
            const updatedFavorites = favorites.filter(item => item.article !== data.article);
            localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
            setIsFavorite(false);
            const newFavoriteItems = favoriteItems > 0 ? favoriteItems - 1 : 0;
            setFavoriteItems(newFavoriteItems);
            localStorage.setItem('numberOfFavorites', newFavoriteItems);
        } else {
            const updatedFavorites = [...favorites, data];
            localStorage.setItem('favorites', JSON.stringify(updatedFavorites));
            setIsFavorite(true);
            const newFavoriteItems = favoriteItems + 1;
            setFavoriteItems(newFavoriteItems);
            localStorage.setItem('numberOfFavorites', newFavoriteItems);
        }
    };

    //cart

    useEffect(() => {
        const itemsInCart = JSON.parse(localStorage.getItem('cartItems')) || [];
        setIsAddedToCart(itemsInCart.includes(data.article))
    }, [data.article]);

    useEffect(() => {
        const storeditemsInCart = localStorage.getItem('numberOfCartItems') || 0;
        if (storeditemsInCart !== null) {
            setCartItems(parseInt(storeditemsInCart));
        }
    }, []);

    const addToCartHandler = () => {
        const cartItemsFromStorage = JSON.parse(localStorage.getItem('cartItems')) || [];
        const isAlreadyInCart = cartItemsFromStorage.some(item => item.article === data.article);
        if (isAlreadyInCart) {
            console.log('already in the cart', data.article);
        } else {
            const updatedCartItems = [...cartItemsFromStorage, data];
            localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
            setCartItems(updatedCartItems.length);
            localStorage.setItem('numberOfCartItems', updatedCartItems.length);
            setIsAddedToCart(true)
        }
    };

    const {
        name,
        price,
        url,
        color,
    } = data
    return (
        <li className="products-list-item">
            <img src={url} className="products-list-item-img" />
            <h1>{name} <br /><span>{price} грн</span></h1>
            <p>Колір: {color}</p>
            <div className="buttons-wrapper">
                <Button onClick={() => setAddToCart(!addToCart)} type='button' className="add-to-cart-btn" >У кошик</Button>

                <Button onClick={toggleFavorite} className={`favorite-btn ${isFavorite ? 'active' : ''}`}>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                        <path fill={isFavorite ? 'black' : 'none'} stroke="black" strokeWidth="2" d="M12 1.154l2.478 7.618h8.022l-6.477 4.707 2.478 7.618-6.477-4.707-6.477 4.707 2.478-7.618-6.477-4.707h8.022z" />
                    </svg>
                </Button>
            </div>

            {addToCart &&
                <FirstModal
                    onClose={handleCloseFirstModal}
                    image={url}
                    setCartItems={setCartItems}
                    cartItems={cartItems}
                    onAdd={addToCartHandler}
                />}


        </li >
    )
}

Product.propTypes = {
    data: PropTypes.object,
    setCartItems: PropTypes.func,
    setFavoriteItems: PropTypes.func,
    favoriteItems: PropTypes.number,
    cartItems: PropTypes.number,
}

export default Product