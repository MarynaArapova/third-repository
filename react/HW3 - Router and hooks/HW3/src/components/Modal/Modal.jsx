import React from "react";
import ModalWrapper from "./ModalWrapper.jsx";
import ModalHeader from "./ModalHeader.jsx";
import ModalFooter from "./ModalFooter.jsx";
import ModalClose from "./ModalClose.jsx";
import ModalContent from "./ModalContent.jsx";

import "./Modal.scss"

const Modal = ({ }) => {

    return (
        <ModalWrapper>

            <ModalClose />
            <ModalHeader>Base Modal</ModalHeader>
            <ModalContent>
            </ModalContent>
            <ModalFooter>
            </ModalFooter>
        </ModalWrapper>

    )
}

export default Modal