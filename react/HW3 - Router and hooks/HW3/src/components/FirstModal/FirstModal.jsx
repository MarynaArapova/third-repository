import React from "react";
import PropTypes from "prop-types";

import ModalBody from '../Modal/ModalBody.jsx';
import ModalClose from '../Modal//ModalClose.jsx';
import ModalFooter from '../Modal//ModalFooter.jsx';
import ModalHeader from '../Modal//ModalHeader.jsx';
import ModalWrapper from '../Modal//ModalWrapper.jsx';
import ModalContent from '../Modal//ModalContent.jsx';

const FirstModal = ({ onClose, image, setCartItems, cartItems, onAdd }) => {
    return (
        <ModalWrapper onClick={onClose}>
            <ModalBody>
                <ModalHeader>
                    <ModalClose onClick={onClose} />
                </ModalHeader>
                <ModalContent>
                    <img src={image} className="modal-image" />
                    <h1>Додати товар у кошик?</h1></ModalContent>
                <ModalFooter
                    firstText="Так"
                    secondaryText="Ні"
                    setCartItems={setCartItems}
                    cartItems={cartItems}
                    onClick={onAdd}
                >
                </ModalFooter>

            </ModalBody>
        </ModalWrapper >
    )
}

FirstModal.propTypes = {
    onClose: PropTypes.func,
    image: PropTypes.string,
    setCartItems: PropTypes.func,
    cartItems: PropTypes.number,
}

export default FirstModal
