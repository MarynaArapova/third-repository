import React from 'react';
import { useState, useEffect } from 'react'
import ProductsList from './components/ProductsList/ProductsList.jsx';
import { sendRequest } from './helpers/sendRequest.js';
import RootRoters from './routers'

import './App.scss'
import PageHeader from './components/PageHeader/PageHeader.jsx';

function App() {

  const [showFirstModal, setShowFirstModal] = useState(false);
  const [productsList, setProductsList] = useState([]);

  const [addToCart, setAddToCart] = useState(false)
  const [isFavorite, setIsFavorite] = useState(false);

  // 
  const [isAddedToCart, setIsAddedToCart] = useState(false)
  // 
  const [cartItems, setCartItems] = useState(
    parseInt(localStorage.getItem('numberOfCartItems')) || 0
  );
  const [favoriteItems, setFavoriteItems] = useState(
    parseInt(localStorage.getItem('numberOfFavorites')) || 0
  );


  const handleCloseFirstModal = () => {
    setShowFirstModal(false);
  };


  useEffect(() => {
    sendRequest('../public/products.json')
      .then((data) => { setProductsList(data) })
  }, []);


  return (
    <>
      <PageHeader
        cartItems={cartItems}
        favoriteItems={favoriteItems} />
      <RootRoters cartItems={cartItems}
        favoriteItems={favoriteItems}
        productsList={productsList}
        setCartItems={setCartItems}
        setFavoriteItems={setFavoriteItems}
        addToCart={addToCart}
        setAddToCart={setAddToCart}
        isAddedToCart={isAddedToCart}
        setIsAddedToCart={setIsAddedToCart}
        isFavorite={isFavorite}
        setIsFavorite={setIsFavorite} />
    </>
  )
}

export default App
