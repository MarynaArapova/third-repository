// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// це технологія, що дозволяє веб-сторінкам взаємодіяти з сервером асинхронно, тобто без оновлення сторінки.

const API = 'https://ajax.test-danit.com/api/swapi/films';
const filmList = document.createElement('ul');

const films = fetch(API)
    .then((response) => {
        return response.json();
    })
    .then((films) => {

        const loadCharacters = (film) => {
            const charactersPromises = film.characters.map(character => fetch(character).then(response => response.json()));
            return Promise.all(charactersPromises);
        };

        films.forEach(film => {
            const { episodeId, name, openingCrawl } = film;
            const filmListItem = document.createElement('li');
            filmListItem.textContent = `#${episodeId}: ${name}. ${openingCrawl}`
            filmList.append(filmListItem);
            loadCharacters(film)
                .then(characters => {
                    const charactersListItem = document.createElement('p');
                    charactersListItem.textContent = `Characters: ${characters.map(character => character.name).join(', ')}`;
                    filmListItem.append(charactersListItem);
                });

        });
    });

document.body.append(filmList)


