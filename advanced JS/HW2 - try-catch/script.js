// Теоретичне питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// Непередбачувані вхідні дані від користувачів, неправильні відповіді від сервера, помилки при розрахунках

// Завдання

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.querySelector('#root');

const list = document.createElement('ul');
books.forEach(book => {
    try {
        const { author, name, price } = book;

        if (!author) {
            throw new Error(`Missing 'author' property in book`);
        } else if (!name) {
            throw new Error(`Missing 'name' property in book`);
        } else if (!price) {
            throw new Error(`Missing 'price' property in book`);
        }
        let li = document.createElement('li');
        li.textContent = `${author}: ${name}, Price: ${price}`;
        list.appendChild(li)
    }
    catch (error) {
        console.log(error.message);
    }
})

root.append(list)
