// Теоретичне питання

// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Це дозволяє об'єктам унаслідувати властивості і методи від інших об'єктів.  Всі екземпляри створеного об'єкта унаслідують властивості 
// і методи цього прототипу.

// Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Щоб викликати конструктор класу-батька та ініціалізувати його властивості до того, як будуть ініціалізовані 
// властивості класу-нащадка.

class Employee {
    constructor(name = '', age = '', salary = '') {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value) {
        this._name = value;
    }

    get name() {
        return this._name;
    }

    set age(value) {
        this._age = value;
    }

    get age() {
        return this._age;
    }

    set salary(value) {
        this._salary = value;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this._lang = lang;
    }

    set lang(value) {
        this._lang = value;
    }

    get lang() {
        return this._lang;
    }

    get salary() {
        return super.salary * 3;
    }
}

const programmer1 = new Programmer("John", 30, 50000, ["Python", "JavaScript"]);
const programmer2 = new Programmer("Alice", 25, 60000, ["Java", "C++"]);

console.log("Programmer 1:");
console.log("Name:", programmer1.name);
console.log("Age:", programmer1.age);
console.log("Salary:", programmer1.salary);
console.log("Languages:", programmer1.lang);
console.log();
console.log("Programmer 2:");
console.log("Name:", programmer2.name);
console.log("Age:", programmer2.age);
console.log("Salary:", programmer2.salary);
console.log("Languages:", programmer2.lang);
