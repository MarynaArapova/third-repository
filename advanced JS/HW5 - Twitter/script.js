const root = document.querySelector('.root');

class Card {
    constructor(title, text, name, lastName, email, id) {
        this.title = title;
        this.text = text;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.id = id;
    }
}

const loaderContainer = document.createElement('div');
loaderContainer.classList.add('loader-container');
loaderContainer.innerHTML = `<div class='loader'><span></span>
<span></span>
<span></span></div>`
root.appendChild(loaderContainer);

fetch('https://ajax.test-danit.com/api/json/users')
    .then((response) => { return response.json() })
    .then((usersList) => {

        fetch('https://ajax.test-danit.com/api/json/posts')
            .then((response) => { return response.json() })
            .then((postsList) => {

                setTimeout(() => {
                    loaderContainer.remove();

                    postsList.forEach(post => {
                        const author = usersList.find((el) => el.id === post.userId)

                        const card = new Card(post.title, post.body, author.name, author.username, author.email, post.id);
                        const cardWrapper = document.createElement('div');
                        cardWrapper.id = card.id;

                        const { name, title, lastName, text, email } = card;
                        cardWrapper.classList.add('card-style');
                        cardWrapper.innerHTML =
                            `<button class="close-btn">X</button>
                        <p class='name'>${lastName} <span>@${name}</span></p>
                        <h1> ${title}</h1>
                        <p class='text'>${text}</p>
                        <p>Email:<a href="#">${email}</a></p>
                        `

                        root.append(cardWrapper);
                    });
                }, 2000);
            });
    });

root.addEventListener('click', (event) => {
    if (event.target.classList.contains('close-btn')) {
        const cardWrapper = event.target.closest('.card-style');
        deletePost(cardWrapper.id, cardWrapper)

    }
});

function deletePost(postId, cardWrapper) {
    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
        method: 'DELETE'
    })
        .then(response => {
            if (response.ok) {
                cardWrapper.remove();
            } else {
                console.error('Помилка видалення поста');
            }
        })
        .catch(error => {
            console.error('Помилка видалення поста:', error);
        });
}
