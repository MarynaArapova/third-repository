// Теоретичне питання

// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// Це можливість виконання деяких операцій без очікування завершення попередніх операцій. 
// В асинхронному коді операції можуть запускатися паралельно, і програма може продовжувати виконувати інші дії, не чекаючи завершення 
// асинхронної операції.

const btn = document.querySelector('.btn');
const ipInfo = document.querySelector('.ipInfo');

const IP = 'https://api.ipify.org/?format=json';

btn.addEventListener('click', async () => {
    const ip = await fetch(IP);
    const ipData = await ip.json();
    const ipAdress = ipData.ip;
    const userRequest = await fetch(`http://ip-api.com/json/${ipAdress}`);
    const userInfo = await userRequest.json();
    console.log(userInfo);
    for (const key in userInfo) {
        ipInfo.innerHTML += `${key}: ${userInfo[key]}<br>`
        console.log(`${key}: ${userInfo[key]}`);
    }
})
